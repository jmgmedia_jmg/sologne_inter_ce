<?php
/***************************************************************************************
* Warning !! CMCIC_Config contains the key, you have to protect this file with all     *   
* the mechanism available in your development environment.                             *
* You may for instance put this file in another directory and/or change its name       *
***************************************************************************************/

define ("CMCIC_CLE", "7664B5BB1CFCE437FCB653D6B7B37DE9BE74A4PA");
define ("CMCIC_TPE", "0369679");
define ("CMCIC_VERSION", "3.0");
define ("CMCIC_SERVEUR", "https://paiement.creditmutuel.fr/");
define ("CMCIC_CODESOCIETE", "sologneint");
define ("CMCIC_URLOK", "http://www.sologneinterce.net/retour_boutique.php?pok");
define ("CMCIC_URLKO", "http://www.sologneinterce.net/retour_boutique.php?pko");
?>
