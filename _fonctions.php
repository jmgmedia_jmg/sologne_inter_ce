<?php

ini_set("display_errors", 1);


$taux_tva = 0;


setlocale(LC_ALL, 'fr_FR');


function send_sql($sql,$voir=false) {
	global $db;
	if (!$res=mysqli_query($db,$sql)){
		echo mysqli_error($db)."<br /><b>".$sql."</b>";
		exit;
		}
	else
		if ($voir)
			echo "<br /><b>".$sql."</b>";
	return $res;
	}



function datefr($dateus) {
  if ($dateus!=""&&$dateus!="0000-00-00") {
    list($aa,$mm,$jj)=explode("-",$dateus);
    return $jj."/".$mm."/".$aa;
    }
  else return "";
  }


function dateus($datefr) {
  if ($datefr!=""&&$datefr!="00/00/0000") {
    list($jj,$mm,$aa)=explode("/",$datefr);
    return $aa."-".$mm."-".$jj;
    }
  else return "";
  }



function renomme_fichier($string) {
	/*
      $string=str_replace(" ","_",StripSlashes($string));
      $string=str_replace("/","", $string);
      $string=str_replace("'","_",$string);
      $string=str_replace("�","o",$string);
      $string=str_replace("+","_",$string);
      $string=str_replace("&","_",$string);
      $string=strtr($string,'���������������������������������������������������',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
*/
	$string = strtr($string,'���������������������������������������������������',
	'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	$string = preg_replace('/([^.a-z0-9]+)/i', '_', $string); 
	return strtolower($string);
	}


function envoi_message($dest,$titre,$message,$expediteur='contact@sologneinterce.net',$mode='html',$chemin="./") {

	global $var_adresse_site,$nom_marchand,$var_signature;
	
	require_once('phpmailer/class.phpmailer.php');
	$mail = new PHPMailer(); // defaults to using php "mail()"
	$mail->FromName = $nom_marchand;
	$mail->From = $expediteur;

	$mail->AddReplyTo('contact@sologneinterce.net',$nom_marchand);
	//$mail->AltBody    = "Pour lire ce message, utilisez un client mail HTML !"; // optional, comment out and test
	$mail->AltBody    = $message;
	$mail->Subject    = $titre;
	$mail->AddAddress($dest);

	$header = file_get_contents($chemin.'mail-header.php');
	$footer = file_get_contents($chemin.'mail-footer.php');

	$body = $header.'<p>'.nl2br($message).'</p>'.$footer;
	
	$mail->MsgHTML($body);
	
	/*
	$mail->DKIM_domain = 'sologneinterce.net';
	$mail->DKIM_private = '.htkeyprivate';
	$mail->DKIM_selector = 'phpmailer';
	$mail->DKIM_passphrase = '1478251857';
	*/
	if(!$mail->Send())
		echo "Mailer Error: " . $mail->ErrorInfo;
	else
		send_sql("INSERT INTO messages(DEST,TITRE,MESSAGE) 
			VALUES (\"$dest\",\"".addslashes($titre)."\",\"".addslashes($message)."\")");
	}

/*
function url_refer($in) {
	$string=strtr($in,' ���������������������������������������������������',
'-aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
	return preg_replace('�[^a-zA-Z0-9\-]*�','', strtolower($string));
	}
*/
?>