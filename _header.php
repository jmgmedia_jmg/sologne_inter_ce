<?php

require_once '_init.php';

$class_logo_wrapper = '';

if (isset($page))
	if ($page=='home')
		$class_logo_wrapper = 'home-logo-wrapper';
	
	
ob_start();
$res = send_sql("SELECT * FROM categories ORDER BY LIBELLE");
while ($ligne=mysqli_fetch_array($res))
{
	$res2 = send_sql("SELECT * FROM produits WHERE CATEG='".$ligne['ID']."' AND AFFICHER=1");
	$nbenr = mysqli_num_rows($res2);
	if ($nbenr>0) { ?>
	
		<li><a href="offres.php?categ=<?=$ligne['ID']?>"><?=majuscules(stripslashes($ligne['LIBELLE']))?></a></li>
		
	<?php
	}
}
$liste_categories = ob_get_clean();


?><!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- SITE META -->
    <title>Sologne Inter CE - Votre partenaire en Région Centre</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">

    <!-- TEMPLATE STYLES -->
	<?php if (isset($use_fancybox)) { ?>
		<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
	<?php } ?>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/flexslider.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

    <!-- START SITE -->

    <div id="wrapper">
        <div class="container">
		  
        <div class="logo-wrapper <?=$class_logo_wrapper?>">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
                    </div>
                    <!-- end col -->
                    <div class="col-md-9 col-sm-12">
									
                            <ul class="header-r-cart pull-right">
									 
										<?php if (isset($_SESSION['solognece_client'])) { ?>
										
											<?=$client_nom?><br />
											<a href="<?=$_SERVER["PHP_SELF"]."?".$_SERVER["QUERY_STRING"].($_SERVER["QUERY_STRING"]!=""?"&":"")?>dcnx">se déconnecter</a><br />
											
										<?php } ?>

									 

                            </ul>

									
									
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
			  </div>
			  <!-- end logo-wrapper -->

        <header class="header">
                <nav class="navbar navbar-default yamm">
                    <div class="container-full">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="index.php"><i class="fa fa-home"></i> Accueil</a></li>
                                <li class="dropdown hasmenu hidden-lg hidden-md">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nos offres <span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu">
													
													<?=$liste_categories?>

                                    </ul>
                                </li>
                                <li><a href="actu.php">Actualités</a></li>
                                <li><a href="qui_sommes.php">Qui sommes-nous ?</a></li>
                                <li><a href="commande.php">Commander</a></li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right searchandbag">
                                <li class="dropdown searchdropdown hasmenu">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart"></i></a>
                                    <ul class="dropdown-menu show-right">
										<?php
										
										$res_cart = send_sql("SELECT * FROM vel_commandes WHERE SESSION='$id_client'");
										$nbprod = mysqli_num_rows($res_cart);
										
										?>

                                <li><a class="cart" href="panier.php"><?php if ($nbprod>0) { ?>Votre panier :<?php } else {?>
											Votre panier est vide
											<?php } ?>
											</a>
											<?php if ($nbprod>0) { ?>
                                 <div class="mini-cart-content">
											<?php
											$total_com = 0;
											while ($ligne_cart=mysqli_fetch_array($res_cart))
											{
												$lien = '#';
												$panier_idprod = $ligne_cart['IDPROD'];
												$panier_nom = stripslashes($ligne_cart['NOM']);
												$panier_qte = $ligne_cart['QTE'];
												$panier_total = paszero($ligne_cart['TOTALHT']);
												// PHOTO PRODUIT OU NON...
												$panier_img = "offres/$panier_idprod/".renomme_fichier($panier_nom)."_g.jpg";
												if (!file_exists($panier_img)) 
													$panier_img = "images/pasphoto.jpg";
												$taille = GetImageSize($panier_img);
												$imgl = $taille[0];
												$imgh = $taille[1];
												
												$lien = 'offres.php?categ=1&id='.$panier_idprod;
												?>

                                        <div class="mini-top-sec">
															<!--
                                            <div class="cart-image">
                                                <a href="<?=$lien?>"><img alt="<?=$panier_nom?>" src="<?=$panier_img?>"></a>
                                            </div>
														  -->
                                            <div class="cart-info">
                                                <a href="<?=$lien?>"><h4><?=$panier_qte?> x <?=$panier_nom?></h4></a>
                                                <h5 class="pull-left">Prix</h5>
																<h5 class="pull-right"><?=$panier_total?>&nbsp;€</h5>
                                            </div>
                                            <!-- <div class="product-cancel"><a href="#"><i class="fa fa-times"></i></a></div> -->
                                        </div>
													 
											<?php
											$total_com = $total_com + $ligne_cart['TOTALHT'];
											}
											?>
                                        <div class="mini-top-sec">
                                            <h4 class="pull-left">Total</h4>
														  <h4 class="pull-right"><?=paszero($total_com)?>&nbsp;€</h4>
                                        </div>
                                        <div class="mini-top-sec cart-commander">
                                            <a class="btn btn-primary" href="panier.php">COMMANDER</a>
													</div>
											<?php } ?>
                                </li>
										  </ul>
                                </li>
                                <li class="dropdown searchdropdown hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
                                    <ul class="dropdown-menu show-right">
                                        <li>
                                            <div id="custom-search-input">
																<form method="post" action="recherche.php">
                                                <div class="input-group col-md-12">
                                                    <input name="CH_RECH" type="text" class="form-control input-lg" placeholder="Votre recherche..." />
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-lg" type="submit">
                                                            <i class="fa fa-search submit"></i>
                                                        </button>
                                                    </span>
                                                </div>
																</form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container-fluid -->
                </nav>
        </header>
        <!-- end header -->

        <div class="sitecontainer bgw">
		  
		  
				<!-- ACTUS -->
				<?php if (isset($bandeau_actu)) { ?>
				
            <div class="row hidden-xs">
                <div class="col-md-12">
                    <div class="news-ticker clearfix">
                        <div class="news-title">
                            <h3>Les actus de Sologne Inter CE</h3>
                        </div>
                        <ul id="ticker">
								
								<?php
								$res=send_sql("SELECT * FROM actu WHERE AFFICHER=1 ORDER BY DATE DESC LIMIT 8");
								while ($ligne=mysqli_fetch_array($res))
								{
									$accroche = ucfirst(stripslashes($ligne['ACCROCHE'])); 
									?>
                            <li><a href="actu.php?ref=<?=$ligne['REF']?>"><?=$accroche?> [..] - <span><?=datefr($ligne['DATE'])?></span></a></li>
								<?php } ?>
								
                        </ul>
                        <div class="random clearfix">
                            <a href="actu.php" data-toggle="tooltip" data-placement="bottom" title="Toutes les actualités">
                                <span class="random-article">
                                <i class="fa fa-random"></i>
                            </span>
                            </a>
                        </div>
                        <!-- end random -->
                    </div>
                    <!-- end news-ticker -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
				
				<?php } ?>

            <div class="row">
				
                <div class="col-md-3 hidden-sm hidden-xs">

                    <div class="widget hidden-xs">
                        <div class="widget-title">
                            <h4>Nos offres</h4>
                            <hr>
								</div>
							
                                <ul class="category">
										  
										  <?=$liste_categories?>
										  
                                </ul>
							</div>
							<!-- end Widget -->
						</div>
						<!-- end sidebar -->
