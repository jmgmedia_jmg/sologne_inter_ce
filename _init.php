<?php

require_once '_fonctions.php';


if ($_SERVER['SERVER_NAME']=="localhost") {
	$dbserveur = "localhost";
	$dbident = "root";
	$dbpasswd = "";
	$dbname = "sologne_inter_ce";
	$dossier_actus = 'actu/';
	$dossier_offres = 'offres/';
	}
else {
	$prefixe_tables = '';
	$dbserveur = "mysql5-10.60gp";
	$dbident = "solognei";
	$dbpasswd = "sol41ce";
	$dbname = "solognei";
	$dossier_actus = '../actu/';
	$dossier_offres = '../offres/';
	}



if (!$db = mysqli_connect($dbserveur,$dbident,$dbpasswd,$dbname))
	echo "Erreur de connexion à la base de données !!!";
else
	send_sql("SET NAMES UTF8");


$dossier_vel = "/";
$taux_tva = 0.196;
$var_home = $_SERVER['DOCUMENT_ROOT'];
$var_banque = "CREDIT MUTUEL";
$var_delai_expe = "48 heures";
$nom_marchand = "Sologne Inter CE";
$email_commandes = "Sologne Inter CE";



if (!session_id()) {
    session_start();
    $id_client = session_id();
    }
else
    if (!isset($id_client))
        $id_client = session_id();

if (isset($_SESSION["solognece_ce"]))
	$solognece_ce = $_SESSION["solognece_ce"];


if (isset($_GET['dcnx']))
{
	$solognece_client = "";
	unset($_SESSION['solognece_client']);
}





if (isset($_SESSION["solognece_client"])) {
    $solognece_client = $_SESSION["solognece_client"];
    $selection = send_sql("SELECT * FROM vel_clients WHERE REF='$solognece_client' AND CONFIRM=1");
    if ($ligne = mysqli_fetch_array($selection)) {
		$client_email = $ligne['EMAIL'];
		$client_civilite = $ligne['CIVILITE'];
		$client_nom=stripslashes($ligne['NOM']);
		$client_prenom=stripslashes($ligne['PRENOM']);
		$client_societe=stripslashes($ligne['SOCIETE']);
		$client_points_parrain=$ligne['POINTS_PARRAIN'];
		}
	}







$email_contact = "contact@jmgmedia.com";
$res = send_sql("SELECT CONTENU FROM param WHERE RUBRIQUE='EMAIL_CONTACT'");
if ($ligne = mysqli_fetch_array($res))
	$email_contact = StripSlashes($ligne['CONTENU']);


// lecture de l'adresse e-mail pour envoi des commandes
$email_commandes = $email_contact;
$res=send_sql("SELECT CONTENU FROM param WHERE RUBRIQUE='EMAIL_COMMANDES'");
if ($ligne=mysqli_fetch_array($res))
	$email_commandes = $ligne['CONTENU'];


// Coordonnées
$nos_coordonnees = '';
$res = send_sql("SELECT CONTENU FROM param WHERE RUBRIQUE='NOS_COORDONNEES'");
if ($ligne = mysqli_fetch_array($res))
	$nos_coordonnees = nl2br($ligne['CONTENU']);




function paszero($nombre,$dec=2)
{
	return ($nombre!=0?number_format($nombre,$dec,',',' '):'');
}


function genere_password($length) {
        $mauvais_chars = array(58,59,60,61,62,63,64);
        $var="";
        mt_srand(time());
        while (strlen($var) < $length) {
                $tmp = mt_rand(48,90);
                if (in_array($tmp,$mauvais_chars))
                        continue;
                $var .= chr($tmp);
                }
        return strtolower($var);
        }
	
$rep_photos="";

function url_filter($in) {
    $search = array ('@&#([0-9]+);@','[À]','[à]','[á]','[ã]','[â]','[ä]','[å]','[Á]','[Â]','[Ã]','[Ä]','[Å]','[È]','[É]','[Ë]','[Ê]','[é]','[è]','[ê]','[ë]','[Î]','[Ï]','[ï]','[î]','@[ìíÌÍ]@i','@[ùúûüÚÛÜ]@i','[Ù]','[Ô]','@[òóôõöøÒÓÕÖØ]@i','[Ç]','[ç]');
    $replace = array ('-','a','a','a','a','a','a','a','a','a','a','a','a','e','e','e','e','e','e','e','e','i','i','i','i','i','u','u','o','o','c','c');
    return preg_replace($search, $replace, strtolower($in));
    }


function ident_filter($in) {
    $search = array ('@&#([0-9]+);@','[ ]','[-]','[\']','[À]','[à]','[á]','[ã]','[â]','[ä]','[å]','[Á]','[Â]','[Ã]','[Ä]','[Å]','[È]','[É]','[Ë]','[Ê]','[é]','[è]','[ê]','[ë]','[Î]','[Ï]','[ï]','[î]','@[ìíÌÍ]@i','@[ùúûüÚÛÜ]@i','[Ù]','[Ô]','@[òóôõöøÒÓÕÖØ]@i','[Ç]','[ç]');
    $replace = array ('','','','','a','a','a','a','a','a','a','a','a','a','a','a','e','e','e','e','e','e','e','e','i','i','i','i','i','u','u','o','o','c','c');
    return preg_replace($search, $replace, strtolower($in));
    }


function renomme_photo($string) {
      $string=str_replace(" ","_",StripSlashes($string));
      $string=str_replace("/","", $string);
      $string=str_replace("'","_",$string);
      $string=str_replace("°","o",$string);
      $string=str_replace("+","_",$string);
      $string=strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');

      return strtolower($string);
      }

function renomme_url($string) {
      $string=str_replace(" ","-",StripSlashes($string));
      $string=str_replace("/","", $string);
      $string=str_replace("'","-",$string);
      $string=str_replace("°","o",$string);
      $string=str_replace("+","-",$string);
      $string=strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');

      return strtolower($string);
      }




function url_refer($in) {
//header('Content-type: text/html; charset=utf-8');
	$a = ' /àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ';
	$b = '--aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY';
	$string = utf8_encode(strtolower(strtr(utf8_decode($in), utf8_decode($a), utf8_decode($b))));
	return utf8_encode(preg_replace(utf8_decode('‘[^a-zA-Z0-9\-]*‘'), '', utf8_decode($string))) ;
	}


function majuscules($string) {
	$string = utf8_encode(strtr(utf8_decode($string),utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿ'),utf8_decode('aaaaaceeeeiiiinooooouuuuyy')));
	return strtoupper($string);
	}

?>