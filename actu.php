<?php

$use_fancybox = true;

include '_header.php';

$ligne_intro = "Les news de Sologne Inter CE";

?>

<div class="col-md-9 col-sm-12">

	<div class="widget-title">
		 <h4>Les news de Sologne Inter CE</h4>
		 <hr>
	</div>



<?php

$exclu_actu = '';

if (isset($_REQUEST['ref']))
{
	$ref = $_REQUEST['ref'];
	$exclu_actu = " AND REF!='$ref'";
	$res = send_sql("SELECT * FROM actu WHERE AFFICHER=1 AND REF='$ref'");
	if ($ligne=mysqli_fetch_array($res))
	{
		$date = datefr($ligne['DATE']);
		$accroche = stripslashes($ligne['ACCROCHE']);
		$titre = stripslashes($ligne['TITRE']);
		$resume = stripslashes($ligne['RESUME']);
		$texte = stripslashes(nl2br($ligne['TEXTE']));

		$ligne_intro = $titre;

		$visuel = $dossier_actus.$ref.'.jpg';
		if (!file_exists($visuel)) 
			$visuel = "images/pasphoto.jpg";

		?>
			  <div class="widget searchwidget indexslider">
					<div class="large-widget m30">
						 <div class="post row clearfix">
							  <div class="col-md-5">
									<div class="post-media">
										 <a class="fancybox" href="<?=$visuel?>">
											  <img alt="" src="<?=$visuel?>" class="img-responsive">
										 </a>
									</div>
							  </div>

							  <div class="col-md-7">
									<div class="title-area">
										 <div class="colorfulcats">
											  <a href="#"><span class="label label-warning">Actus</span></a>
										 </div>
										 <h3><?=$titre?></h3>
										 
										<?=$resume?>
										<?=$texte?>

										 <div class="large-post-meta">
											  <span class="avatar">Sologne Inter CE</span>
											  <small>&#124;</small>
											  <span><i class="fa fa-clock-o"></i> <?=$date?></span>
											  <!--
											  <small class="hidden-xs">&#124;</small>
												<span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
												-->
										</div>
												
										 <!-- end meta -->
									</div>
									<!-- /.pull-right -->
							  </div>
						 </div>
						 <!-- end post -->
					</div>
					<!-- end large-widget -->

			  </div>
			  <!-- end widget -->

<?php

	}
}


$res = send_sql("SELECT * FROM actu WHERE AFFICHER=1$exclu_actu ORDER BY DATE DESC");
$nbprod = mysqli_num_rows($res);

if ($nbprod>0) { ?>

				<div class="widget searchwidget indexslider">
				
	<?php

	while ($ligne=mysqli_fetch_array($res))
	{
		$ref = $ligne['REF'];
		$date = datefr($ligne['DATE']);
		$titre = stripslashes($ligne['TITRE']);
		$resume = stripslashes($ligne['RESUME']);
		$visuel = $dossier_actus.$ref.'.jpg';
		if (!file_exists($visuel)) 
			$visuel = "images/pasphoto.jpg";
		?>
                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-3">
                                    <div class="post-media">
                                        <a href="actu.php?ref=<?=$ref?>">
                                            <img alt="" src="<?=$visuel?>" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-warning">Actus</span></a>
                                        </div>
                                        <h3><?=$titre?></h3>
													 <p><?=$resume?></p>

                                        <div class="large-post-meta">
                                            <span class="avatar">Sologne Inter CE</span>
                                            <small>&#124;</small>
                                            <span><i class="fa fa-clock-o"></i> <?=$date?></span>
														  <!--
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
														  -->
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->
	<?php
	
	}
}

?>


                    </div>
                    <!-- end widget -->

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="ads-widget">
                                    <a href="#"><img src="upload/banner_02.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <!-- end ads-widget -->
                            </div>
                            <!-- end widget -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- end col -->



<?php

include '_footer.php';

?>
