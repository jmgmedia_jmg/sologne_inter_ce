<?php

require_once('../_init.php');


// Ne garder que le stock et les vendeurs pour le CRON
// Les installations programmées et non programmées ne doivent être utilisées qu'une seule fois

function creerTableau($res)
{
	$array = array();
	while($ligne = mysqli_fetch_array($res))
	{
		$adresse = preg_replace("/\r|\n/", "", addslashes($ligne['ADRESSE']));
		
		if($ligne['COORD']!='unknown')
			$array[$ligne['ID']] = $adresse.", ".$ligne['CODEPOST'].' '.addslashes(stripslashes($ligne['VILLE']));
		else
			$array[$ligne['ID']] = $ligne['CODEPOST'].' '.addslashes(stripslashes($ligne['VILLE']));
			
		$stock[$ligne['ID']] = $adresse.', '.$ligne['CODEPOST'].' '.addslashes($ligne['VILLE']);
		$array[$ligne['ID']] .= ", FR";
	}
	return $array;
}


$stock = array();
$res = send_sql("SELECT ID, ADRESSE, CODEPOST, VILLE, COORD  FROM remises WHERE COORD='unknown' OR COORD=''");
$stock = creerTableau($res);



?>

<!-- jQuery  -->
<script type="text/javascript" src="js/jquery.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key= AIzaSyDd0omq6m3nw-9o1FZ8md-WCQaiJUHxgqs" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAavDUrEM1oOixC4mUrNekeBIStb3pOaEY" type="text/javascript"></script>
<script>
	
	var coord = [];
	var geocoder;
	var map;
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(46.227638, 2.213749);
				
		var options = {
			center: latlng,
			zoom: 7,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
				

	
	function boucle_prog (adresse, key) {
		geocoder.geocode({
			"address": adresse
		}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				$.ajax({
					url: "conv_adresse_coord_ajax.php",
					data: "ident=" + key + "&coord=" + results[0].geometry.location,
					asynch: false
				});
			}
			else {
				if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
					setTimeout(function() {
						boucle_prog(adresse, key);
					}, 200);
				}
				else {
					if(status == google.maps.GeocoderStatus.ZERO_RESULTS){
						$.ajax({
							url: "conv_adresse_coord_ajax.php",
							data: "ident=" + key + "&coord=unknown",
							asynch: false
						});
					}
					else
						alert ("fail :" + status);
				}
			}
		});
	}
</script>


<?php

foreach ($stock as $key => $adresse) {	?>

	<script>boucle_prog('<?=$adresse?>', '<?=$key?>');</script>
	
<?php } ?>	



