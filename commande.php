<?php

$use_fancybox = true;

include '_header.php';

?>

<div class="col-md-9 col-sm-12">

	<div class="widget-title">
		 <h4>Nouveau !</h4>
		 <hr>
	</div>

	<br />


	<p>Afin de faciliter les commandes sur notre site, nous avons choisi d'intégrer le paiement en ligne. Vous pouvez donc à présent régler votre commande
	par carte bancaire, de façon sécurisée, avec la plateforme de paiement du Crédit Mutuel CMCIC-Paiement.</p>

	<div class="widget-title">
		 <h4>Comment faire ?</h4>
		 <hr>
	</div>

	<br />

	<p>C'est très simple ! Il vous suffit de parcourir comme d'habitude les offres qui vous intéressent, et vous constaterez
	la présence d'un bouton AJOUTER AU PANIER sous le descriptif de l'offre... n'oubliez pas d'indiquer la quantité !</p>

	<p>Vous remarquerez aussi en haut à droite de la page un lien VOTRE PANIER indiquant le nombre d'articles choisis :
	en cliquant sur le lien, vous accédez au panier...</p>

	<p>Lorsque vous devrez vous identifier pour valider votre commande, vous devrez créer votre compte client si vous ne l'avez
	pas encore fait : un mot de passe personnel vous sera attribué (vous pourrez le changer par la suite).</p>

	<p>Il faudra aussi vous munir du <b>mot de passe de votre CE</b>. Si vous ne l'avez pas encore, contactez votre CE qui vous le transmettra.</p>

	<p>Certaines offres ne peuvent pas être expédiées (les parfums par exemple) et vous devrez venir chercher votre commande dans nos locaux.</p>

	<p>Le paiement par chèque est toujours accepté, mais seulement dans le cas où vous venez chercher votre commande.
	Toute commande envoyée par La Poste (Lettre Max) est obligatoirement réglée par carte bancaire.</p>

	<p>N'hésitez pas à <a href="contact.php">nous contacter</a> si vous rencontrez un problème lors de votre commande.</p>

	

</div>
                <!-- end col -->



<?php

include '_footer.php';

?>
