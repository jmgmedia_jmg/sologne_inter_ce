<?php

$sql="SELECT * FROM vel_clients WHERE REF='$solognece_client'";
$res=send_sql($sql);
$ligne=mysqli_fetch_array($res);
$client_REF=StripSlashes($ligne['REF']);
$client_TYPE=StripSlashes($ligne['TYPE']);
$client_CIVILITE=StripSlashes($ligne['CIVILITE']);
$client_NOM=StripSlashes($ligne['NOM']);
$client_PRENOM=StripSlashes($ligne['PRENOM']);
$client_SOCIETE=StripSlashes($ligne['SOCIETE']);
$client_ADR1=StripSlashes($ligne['ADR1']);
$client_ADR2=StripSlashes($ligne['ADR2']);
$client_CODEPOST=StripSlashes($ligne['CODEPOST']);
$client_VILLE=StripSlashes($ligne['VILLE']);
$client_PAYS=StripSlashes($ligne['PAYS']);
$client_TEL=StripSlashes($ligne['TEL']);
$client_EMAIL=StripSlashes($ligne['EMAIL']);
$client_CIVILITEL=StripSlashes($ligne['CIVILITEL']);
$client_NOML=StripSlashes($ligne['NOML']);
$client_PRENOML=StripSlashes($ligne['PRENOML']);
$client_SOCIETEL=StripSlashes($ligne['SOCIETEL']);
$client_ADR1L=StripSlashes($ligne['ADR1L']);
$client_ADR2L=StripSlashes($ligne['ADR2L']);
$client_CODEPOSTL=StripSlashes($ligne['CODEPOSTL']);
$client_VILLEL=StripSlashes($ligne['VILLEL']);
$client_PAYSL=StripSlashes($ligne['PAYSL']);
$client_TELL=StripSlashes($ligne['TELL']);
$client_REMISE=$ligne['REMISE'];

?>

<SCRIPT TYPE="text/javascript">

function copie_adr() {
  document.form_coord.CH_CIVILITEL[0].checked=document.form_coord.CH_CIVILITE[0].checked;
  document.form_coord.CH_CIVILITEL[1].checked=document.form_coord.CH_CIVILITE[1].checked;
  document.form_coord.CH_CIVILITEL[2].checked=document.form_coord.CH_CIVILITE[2].checked;
  document.form_coord.CH_NOML.value=document.form_coord.CH_NOM.value;
  document.form_coord.CH_PRENOML.value=document.form_coord.CH_PRENOM.value;
  document.form_coord.CH_SOCIETEL.value=document.form_coord.CH_SOCIETE.value;
  document.form_coord.CH_ADR1L.value=document.form_coord.CH_ADR1.value;
  document.form_coord.CH_ADR2L.value=document.form_coord.CH_ADR2.value;
  document.form_coord.CH_CODEPOSTL.value=document.form_coord.CH_CODEPOST.value;
  document.form_coord.CH_VILLEL.value=document.form_coord.CH_VILLE.value;
  document.form_coord.CH_TELL.value=document.form_coord.CH_TEL.value;
}

function valid_form_coord() {

  if (!document.form_coord.CH_CIVILITE[0].checked&&!document.form_coord.CH_CIVILITE[1].checked&&!document.form_coord.CH_CIVILITE[2].checked) {
    alert ("Veuillez sélectionner une civilité");
    return false;}

  if (document.form_coord.CH_NOM.value=="") {
    alert ("Veuillez entrer votre nom");
    document.form_coord.CH_NOM.focus();
    return false;}

  if (document.form_coord.CH_PRENOM.value=="") {
    alert ("Veuillez entrer votre prénom");
    document.form_coord.CH_PRENOM.focus();
    return false;}

  if (document.form_coord.CH_ADR1.value=="") {
    alert ("Veuillez entrer votre adresse");
    document.form_coord.CH_ADR1.focus();
    return false;}

  if (document.form_coord.CH_CODEPOST.value=="") {
    alert ("Veuillez entrer votre code postal");
    document.form_coord.CH_CODEPOST.focus();
    return false;}

  if (document.form_coord.CH_VILLE.value=="") {
    alert ("Veuillez entrer votre ville");
    document.form_coord.CH_VILLE.focus();
    return false;}

  if (document.form_coord.CH_PAYS.value=="") {
    alert ("Veuillez entrer votre pays");
    document.form_coord.CH_PAYS.focus();
    return false;}

  if (document.form_coord.CH_TEL.value=="") {
    alert ("Veuillez entrer votre téléphone");
    document.form_coord.CH_TEL.focus();
    return false;}

  if (document.form_coord.CH_TYPE.value=="choix") {
    alert ("Veuillez indiquer votre catégorie (particulier/professionnel)");
    document.form_coord.CH_TYPE.focus();
    return false;}

  if (!document.form_coord.CH_CIVILITEL[0].checked&&!document.form_coord.CH_CIVILITEL[1].checked&&!document.form_coord.CH_CIVILITEL[2].checked) {
    alert ("Veuillez sélectionner la civilité (livraison)");
    return false;}

  if (document.form_coord.CH_NOML.value=="") {
    alert ("Veuillez entrer le nom (livraison)");
    document.form_coord.CH_NOML.focus();
    return false;}

  if (document.form_coord.CH_PRENOML.value=="") {
    alert ("Veuillez entrer le prénom (livraison)");
    document.form_coord.CH_PRENOML.focus();
    return false;}

  if (document.form_coord.CH_ADR1L.value=="") {
    alert ("Veuillez entrer l'adresse de livraison");
    document.form_coord.CH_ADR1L.focus();
    return false;}

  if (document.form_coord.CH_CODEPOSTL.value=="") {
    alert ("Veuillez entrer le code postal de livraison");
    document.form_coord.CH_CODEPOSTL.focus();
    return false;}

  if (document.form_coord.CH_VILLEL.value=="") {
    alert ("Veuillez entrer la ville de livraison");
    document.form_coord.CH_VILLEL.focus();
    return false;}

  if (document.form_coord.CH_PAYSL.value=="") {
    alert ("Veuillez entrer le pays de livraison");
    document.form_coord.CH_PAYSL.focus();
    return false;}

  if (document.form_coord.CH_PAYSL.value!="France") {
    alert ("Actuellement, nous ne livrons qu'en France. Contactez-nous !");
    document.form_coord.CH_PAYSL.focus();
    return false;}

  if (document.form_coord.CH_NEW_PASSWD.value!=""&&document.form_coord.CH_NEW_PASSWD_CONF.value!=document.form_coord.CH_NEW_PASSWD.value) {
    alert ("La confirmation du nouveau mot de passe n'est pas correcte !");
    document.form_coord.CH_NEW_PASSWD.focus();
    return false;}

  if (document.form_coord.CH_NEW_PASSWD.value!=""&&document.form_coord.CH_NEW_PASSWD.value.length<6) {
    alert ("Votre nouveau mot de passe doit contenir au moins 6 caractères !");
    document.form_coord.CH_NEW_PASSWD.focus();
    return false;}


  document.form_coord.method = "post";
  document.form_coord.action = "<?php echo $page_suivante; ?>";
  return true;
}

</SCRIPT>


<form id="form_coord" name="form_coord" method="POST" onsubmit="return valid_form_coord();">
<input type='hidden' name='CH_TOTAL_COM' value='<?php echo (isset($_POST['CH_TOTAL_COM'])?$_POST['CH_TOTAL_COM']:""); ?>' />
<input type='hidden' name='CH_POIDS_COM' value='<?php echo (isset($_POST['CH_POIDS_COM'])?$_POST['CH_POIDS_COM']:""); ?>' />
<input type='hidden' name='CH_EXPEDIABLE' value='<?php echo (isset($_POST['CH_EXPEDIABLE'])?$_POST['CH_EXPEDIABLE']:""); ?>' />
<input type='hidden' name='CH_REMISE' value='<?php echo $client_REMISE; ?>' />


<p><br />Merci de vérifier vos informations ci-dessous, et les compléter si besoin :</p>


<table id="tabcoord" border="0"  cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <h3><b>Vos coordonnées</b> (facturation)</h3>
      <table border="0" cellspacing="0" cellpadding="0" class=tabl1>
        <tr>
        <td>Civilité :</td>
        <td>
        <table border="0" cellspacing="0" cellpadding="0" class="tabciv">
                <tr>
                        <td><input class="bradio" type="radio" value="Mlle" name="CH_CIVILITE"<?php if ($client_CIVILITE=="Mlle") echo " checked"; ?>></td><td>&nbsp;Mlle </td><td width=15></td>
                        <td><input class="bradio" type="radio" value="Mme"  name="CH_CIVILITE"<?php if ($client_CIVILITE=="Mme")  echo " checked"; ?>></td><td>&nbsp;Mme </td><td width=15></td>
                        <td><input class="bradio" type="radio" value="M."   name="CH_CIVILITE"<?php if ($client_CIVILITE=="M.")   echo " checked"; ?>></td><td>&nbsp;M. </td>
                </tr>
        </table>
        </td>
        </tr>

        <tr><td>Nom :</td><td><input type="text"       name="CH_NOM"          class='grand' value="<?php echo $client_NOM; ?>"></td></tr>
        <tr><td>Prénom :</td><td><input type="text"    name="CH_PRENOM"       class='grand' value="<?php echo $client_PRENOM; ?>"></td></tr>
        <tr><td>Société :</td><td><input type="text"   name="CH_SOCIETE"      class='grand' value="<?php echo $client_SOCIETE; ?>"></td></tr>
        <tr><td>Adresse :</td><td><input type="text"   name="CH_ADR1"         class='grand' value="<?php echo $client_ADR1; ?>"></td></tr>
        <tr><td>&nbsp;</td><td><input type="text"      name="CH_ADR2"         class='grand' value="<?php echo $client_ADR2; ?>"></td></tr>
        <tr><td>Code postal :&nbsp;&nbsp;&nbsp;</td><td><input type="text" name="CH_CODEPOST" class='petit' value="<?php echo $client_CODEPOST; ?>"></td></tr>
        <tr><td>Ville :</td><td><input type="text"     name="CH_VILLE"        class='grand' value="<?php echo $client_VILLE; ?>"></td></tr>
        <tr><td>Pays :</td><td><select name="CH_PAYS" class='grand'><option value="France" selected>France</option></select></td></tr>
        <tr><td>Tél. :</td><td><input type="text"      name="CH_TEL"          class='petit' value="<?php echo $client_TEL; ?>"></td></tr>
<!--
        <tr><td>Catég. :</td><td><select name="CH_TYPE" class='grand'><option
        value="particulier"<?php echo ($client_TYPE=="particulier"?" selected":""); ?>>Particulier</option><option
        value="professionnel"<?php echo ($client_TYPE=="professionnel"?" selected":""); ?>>Professionnel</option></select></td></tr>
-->
		<input type="hidden" name="CH_TYPE" value="particulier" />
      </table>
    </td>

    <td align=right style="vertical-align: middle"><a href="javascript:copie_adr();"><img border="0" src="images/meme_adr.gif" width="58" height="71"></a></td>

    <td style="padding-left: 20px">
      <h3><b>Adresse de livraison</b><span style="font-size: 11px"> [<a href="javascript:copie_adr();">dupliquer les coordonnées</a>]</span></h3>
                          <table border="0" cellspacing="0" cellpadding="0" class="tabl1">
                              <tr>
                                  <td>Civilité :</td>
                                  <td>
                                      <table border="0" cellspacing="0" cellpadding="0" class="tabciv">
                                          <tr>
                                              <td><input class="bradio" type="radio" value="Mlle" name="CH_CIVILITEL"<?php if ($client_CIVILITEL=="Mlle") echo " checked"; ?>></td><td>&nbsp;Mlle </td><td width=15></td>
                                              <td><input class="bradio" type="radio" value="Mme"  name="CH_CIVILITEL"<?php if ($client_CIVILITEL=="Mme")  echo " checked"; ?>></td><td>&nbsp;Mme </td><td width=15></td>
                                              <td><input class="bradio" type="radio" value="M."   name="CH_CIVILITEL"<?php if ($client_CIVILITEL=="M.")   echo " checked"; ?>></td><td>&nbsp;M. </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>

                              <tr><td>Nom :</td><td><input type="text"     name="CH_NOML"          class='grand' value="<?php echo $client_NOML; ?>"></td></tr>
                              <tr><td>Prénom :</td><td><input type="text"  name="CH_PRENOML"       class='grand' value="<?php echo $client_PRENOML; ?>"></td></tr>
                              <tr><td>Société :</td><td><input type="text" name="CH_SOCIETEL"      class='grand' value="<?php echo $client_SOCIETEL; ?>"></td></tr>
                              <tr><td>Adresse :</td><td><input type="text" name="CH_ADR1L"         class='grand' value="<?php echo $client_ADR1L; ?>"></td></tr>
                              <tr><td>&nbsp;</td><td><input type="text"    name="CH_ADR2L"         class='grand' value="<?php echo $client_ADR2L; ?>"></td></tr>
                              <tr><td>Code postal :&nbsp;&nbsp;&nbsp;</td><td><input type="text" name="CH_CODEPOSTL" class='petit' value="<?php echo $client_CODEPOSTL; ?>"></td></tr>
                              <tr><td>Ville :</td><td><input type="text"   name="CH_VILLEL"        class='grand' value="<?php echo $client_VILLEL; ?>"></td></tr>
                                                          <tr><td>Pays :</td><td><select name="CH_PAYSL" class='grand'><option value="France" selected>France</option></select></td></tr>
                              <tr><td>Tél. :</td><td><input type="text"    name="CH_TELL"          class='petit' value="<?php echo $client_TELL; ?>"></td></tr>
                          </table>
    </td>


  </tr>
</table>


<br />
<br />
<p>Si vous souhaitez changer votre mot de passe, entrez le nouveau ci-dessous (et confirmez-le) :</p>
<table border="0" cellspacing="0" cellpadding="0" class=tabl1>
	<tr>
		<td>Nouveau mot de passe :</td>
		<td><input type="password" name="CH_NEW_PASSWD" class="moyen" value="" /></td>
	</tr>
	<tr>
		<td>Confirmation :</td>
		<td><input type="password" name="CH_NEW_PASSWD_CONF" class="moyen" value="" /></td>
	</tr>
</table>



<input type="submit" name="submit" value="Continuer" class="bouton" style="margin: 50px 0">


</form>