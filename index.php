<?php

$page = 'home';

$bandeau_actu = true;

include '_header.php';

?>

                <div class="col-md-6 col-sm-12">
                    <div id="property-slider" class="clearfix">
                        <div class="flexslider">
                            <ul class="slides">
											

<?php

$nb_prio = 0;
$liste_prio = '';

for ($i=1;$i<=5;$i++)
{
	$res = send_sql("SELECT CONTENU FROM param WHERE RUBRIQUE='OFFRE_PRIORITE_$i'");
	$ligne = mysqli_fetch_array($res);
	$res = send_sql("SELECT * FROM produits WHERE ID='".$ligne[0]."'");
	if ($ligne=mysqli_fetch_array($res))
	{
		$tab_prio[] = $ligne['ID'];
		$nb_prio++;
	}
}

if (isset($tab_prio))
	$liste_prio = implode(',',$tab_prio);


$res = send_sql("SELECT * FROM produits WHERE (TOP='1' OR NOUVEAU='1')".($liste_prio!=''?" AND ID NOT IN ($liste_prio)":"")." ORDER BY TOP+NOUVEAU DESC LIMIT ".(10-$nb_prio));
while ($ligne=mysqli_fetch_array($res))
	$tab_prio[] = $ligne['ID'];


if (isset($tab_prio))
{
	foreach ($tab_prio as $id_prod)
	{
		$res = send_sql("SELECT * FROM produits WHERE ID='$id_prod'");
		$ligne=mysqli_fetch_array($res);
		$categ_prod = $ligne['CATEG'];
		$design_prod = StripSlashes($ligne['DESIGNATION']);
		$resume_prod = StripSlashes($ligne['RESUME']);
		//  $visuel="offres/$id_prod"."_p.jpg"; if (!file_exists($visuel)) $visuel="offres/pasphoto.jpg";
		$visuel = "offres/$id_prod/".renomme_fichier($design_prod)."_m.jpg";
		if (!file_exists($visuel)) $visuel="images/pasphoto.jpg";
		?>
                                <li style="background-image: url(<?=$visuel?>)">
                                    <div class="psdesc hidden-xs">
                                        <div class="ps-desc">
                                            <h3><a href="#"><?=$design_prod?></a></h3>
                                            <p><?=$resume_prod?></p>
														  <!--
                                            <span class="type">Technology</span>
                                            <a href="#" class="status"><i class="fa fa-comment-o"></i> 33</a>
														  -->
                                        </div>
                                    </div>
                                    <!-- <a href="#"><img src="upload/big_news_04.jpg" alt="" class="img-responsive"></a> -->
                                </li>
		<?php
	}
}

?>								  
										  
										  
										  
										  
                            </ul>
                            <!-- end slides -->
                        </div>
                        <!-- end flexslider -->
                    </div>
                    <!-- end property-slider -->

                    <div class="widget searchwidget indexslider">
                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_10.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>How ThePhone thriller will change the way film directors & details about the Phone</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_09.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                        </div>
                                        <h3>Time to meet our new designer and developer who joined our team</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_08.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>Best office design's and workspace examples</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_07.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>What you think about our new laptop its build by Apple</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_06.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-success">WordPress</span></a>
                                        </div>
                                        <h3>WordPress App for showcase galleries and Instagram users</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->
                    </div>
                    <!-- end widget -->

                    <div class="widget">
                        <div class="widget-title">
                            <h4>Latest Reviews</h4>
                            <hr>
                        </div>
                        <!-- end widget-title -->

                        <div class="reviewlist review-posts row m30">
                            <div class="post-review col-md-4 col-sm-12 first">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_01.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">MyWatch Review - Its work perfect on mobile?</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                            <div class="post-review col-md-4 col-sm-12">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_02.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">Google Street View Coming to Bhutan</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                            <div class="post-review col-md-4 col-sm-12 last">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_03.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">Mondo, the U.K. banking startup, opens public Beta</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                            <div class="post-review col-md-4 col-sm-12 first">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_04.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">Time to meet new edition Samsung Galaxy S5</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                            <div class="post-review col-md-4 col-sm-12">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_05.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">Apple Watch available on Apple Stores! Did you buy?</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                            <div class="post-review col-md-4 col-sm-12 last">
                                <div class="post-media entry">
                                    <a href="single-review.html" title="">
                                        <img src="upload/review_06.jpg" alt="" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="single-review.html">London Tover Review (Sure in Digital Industrial)</a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

                        </div>

                    </div>
                    <!-- end widget -->

                    <div class="widget searchwidget indexslider">
                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_01.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>How ThePhone thriller will change the way film directors & details about the Phone</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_02.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                        </div>
                                        <h3>Time to meet our new designer and developer who joined our team</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_03.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>Best office design's and workspace examples</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_04.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>What you think about our new laptop its build by Apple</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_05.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-success">WordPress</span></a>
                                        </div>
                                        <h3>WordPress App for showcase galleries and Instagram users</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->
                    </div>
                    <!-- end widget -->

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="ads-widget">
                                    <a href="#"><img src="upload/banner_02.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <!-- end ads-widget -->
                            </div>
                            <!-- end widget -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- end col -->

                <div class="col-md-3 col-sm-12 col-xs-12">

							<?php include '_coldroite.php'; ?>
							
                </div>
                <!-- end col -->


<?php

include '_footer.php';

?>
