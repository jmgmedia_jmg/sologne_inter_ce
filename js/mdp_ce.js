
$(function() {
	
	$("#modal-mdp-ce").on("show.bs.modal", function(e) {
		$('#form_identifiez_vous_txt').html('&nbsp;')
		document.getElementById('form_identifiez_vous').reset();
	})
	;

	$('.bt-valid-mdp-ce').click(function() {
		var okenvoi = true;
		if ($("#CH_MDPVEL").val()=="") {
			$('#form_identifiez_vous_txt').html('<div class="alert alert-danger" role="alert">Vous devez saisir le mot de passe de votre CE !</div>');
			okenvoi = false;
			}
			
		if (okenvoi) {
			$('#form_identifiez_vous_txt').html('');
			$.ajax({
				url: 'form_identifiez_vous_verif.php',
				type: 'post',
				data: $("#form_identifiez_vous").serialize(), 
				/* data: 'id=&votrenom=' + document.form_identifiez_vous.CH_VOTRENOM.value + '&votremail=' + document.formEnvoyerAmi.CH_VOTREMAIL.value + '&mailami=' + document.formEnvoyerAmi.CH_MAILAMI.value + '&texte=' + document.formEnvoyerAmi.CH_TEXTE.value, */
				cache: false,
				success:function(retour){
					if (retour!="") {
						$("#form_identifiez_vous_txt").append('<div class="alert alert-danger" role="alert">' + retour + '</div>');
						}
					else {
						location.reload();
						}
					},
				error: function(XMLHttpRequest, textStatus, errorThrows){
					}
				});
			}
		});

	});
