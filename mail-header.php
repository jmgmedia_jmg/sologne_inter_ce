<html>
<head>
<title>Sologne Inter CE</title>
<style>
body {
	background-color: #ccc;
	font-family: arial, helvetica,sans-serif;
	}
h1 {
	font-size: 18px;
	}
#content, #footer {
	display: block;
	width: 700px;
	width: 670px!important;
	padding: 15px;
	}
#content {
	background-color: #eee;
	color: #000;
	text-align: left;
	font-size: 14px;
	}
#footer {
	background-color: #FF930D;
	color: #fff;
	text-align: center;
	font-size: 12px;
	}
#tabpanier {
	border: 0;
	}
#tabpanier th {
	font-weight: bold;
	font-size: 13px;
	text-align: center;
	border: 0;
	}
#tabpanier td {
	font-weight: normal;
	font-size: 12px;
	border: 0;
	}
#tabpanier th, #tabpanier td {
	padding: 7px 15px;
	vertical-align: middle;
	}
#tabpanier .panpic {
	text-align: center;
	}
#tabpanier td.pandes {
	width: 375px;
	}
#tabpanier .panqte {
	text-align: center;
	}
#tabpanier .panpri {
	text-align: right;
	width: 80px;
	}
#tabpanier .pansto {
	background-color: #ddd; 
	}
#tabpanier .pantot td, #tabpanier th {
	background-color: #067cbc; 
	color: #fff;
	font-weight: normal;
	}
#tabpanier .gro td {
	font-size: 16px;
	}
	
</style>
</head>
<body>

	<img src="http://www.sologneinterce.net/images/entetemail.jpg" alt="Sologne Inter CE"/>

	<div id="content">
	
