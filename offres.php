<?php

if (!isset($_GET['categ']))
	header('location: index.php');

$use_fancybox = true;
$extra_script = 'js/mdp_ce.js';
$extra_script_1 = 'js/offres.js';

include '_header.php';


$retour_panier = '';
include 'panier_ajout.php';



$categ_prod = $_GET['categ'];
$categ_nom = '';
$res = send_sql("SELECT LIBELLE FROM categories WHERE ID='$categ_prod'");
if ($ligne=mysqli_fetch_array($res)) 
	$categ_nom = strtoupper(stripslashes($ligne['LIBELLE']));
else
	exit("<br /><br /><div id='alerte'>Vous n'avez pas accès à cette page</div>");

function page_courante() {
	$fichierCourant = $_SERVER["REQUEST_URI"];
	$parties = explode('/', $fichierCourant );
	return trim($parties[count($parties) - 1]);
	}


?>


                <div class="col-md-9 col-sm-12">

<?php

$sqlmarque = '';

if (isset($_REQUEST['id']))
{
	$id_prod = $_REQUEST['id'];
	$res = send_sql("SELECT * FROM produits WHERE ID='$id_prod'");
	if ($ligne=mysqli_fetch_array($res))
	{
			$design_prod = ucfirst(stripslashes($ligne['DESIGNATION']));
		
			// A t-on le droit de voir le détail de cette offre ? ( rubrique RESTRICTION )
		
			$restriction_prod = $ligne['RESTRICTION'];
		
			if ( $restriction_prod>0 && !isset($solognece_ce) ) { ?>
		
		<br />
		<div id="alerte">Pour voir le contenu de l'offre <strong><?=$design_prod?></strong>, vous devez renseigner le mot de passe de votre CE :<br />
		<br />
		<a href="#" title="Ajouter une galerie photos" data-toggle="modal" data-target="#modal-mdp-ce">Entrer le mot de passe CE</a><br />

		<br />
		</div>
		<br />
		<br />
		<br />
		
		
		<?php } else {

		
		$resume_prod = stripslashes($ligne['RESUME']);
		$details_prod = stripslashes($ligne['DETAILS']);
		
		$alt_title_libelle = str_replace("'","&#146;",$design_prod);
		if ($categ_prod==5) 
			$reserv_prod = "<a href='reservation.php?id=$id_prod'><img border=0 style='margin: 10px 0' src='images/reserv.gif' /></a><br />";
		else 
			$reserv_prod = "";
			
		$visuel = "offres/$id_prod/".renomme_fichier($design_prod)."_g.jpg";
		if (!file_exists($visuel)) 
			$visuel = "images/pasphoto.jpg";

		// -------------------------       FICHIERS   PDF       ----------------------------
		$liste_pdf = '';
		if ($d=@dir($dossier_offres.'/'.$id_prod.'/'))
		{
			while($nomfich=$d->read())
			{
				if (is_file($dossier_offres.'/'.$id_prod.'/'.$nomfich))
				{
					if (strtolower(substr($nomfich,strlen($nomfich)-3,3))=="pdf")
						$liste_pdf .= "<tr><td><img border='0' src='images/pdf.gif'></td><td>&nbsp;".
							"<a href=\"offres/$id_prod/$nomfich\" target='_blank'>$nomfich</a></td></tr>\n";
				}
			}
			$d->close();
		}

		if ($liste_pdf!="") 
			$liste_pdf = "<table border='0' cellpadding='0' cellspacing='0' style='margin-top: 10px'>$liste_pdf</table>";

		?>
                    <div class="widget searchwidget indexslider">
                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a class="fancybox" href="<?=$visuel?>">
                                            <img alt="" src="<?=$visuel?>" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="offres.php?categ=<?=$categ_prod?>"><span class="label label-primary"><?=$categ_nom?></span></a>
                                        </div>
                                        <h3><?=$design_prod?></h3>
													 
													<?=$reserv_prod?>
													<?=$resume_prod?>
													<?=$details_prod?>
													<?=$liste_pdf?>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="offres_imprime.php?id=<?=$id_prod?>" target="_blank"><i class="fa fa-print"></i> Imprimer</a></span>
                                       </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                    </div>
                    <!-- end widget -->

		<?php
	
		$resprix = send_sql("SELECT * FROM refprod WHERE ID_PROD='$id_prod'");
		$numref = mysqli_num_rows($resprix);
		if ($numref>0) : ?>
		
		
		<?php

		if (mysqli_num_rows($resprix)>50)
		{		
			$getmarque = '';
			if (isset($_GET['marque']))
			{
				$getmarque = $_GET['marque'];
				$sqlmarque = " AND MARQUE=\"".$_GET['marque']."\"";
			}

			$resmarques = send_sql("SELECT MARQUE FROM refprod WHERE ID_PROD='$id_prod' GROUP BY MARQUE");
			if (mysqli_num_rows($resmarques)>1)
			{
				echo "<h2>Choisissez une marque : <select onchange=\"location='?categ=$categ_prod&id=$id_prod&marque='+this.options[this.selectedIndex].value\">
					<option value=''>----------</option>\n";
				while ($lmarques=mysqli_fetch_array($resmarques)) {
					if ($sqlmarque=="")
						$sqlmarque = " AND MARQUE=\"".$lmarques[0]."\"";
					echo "<option value=\"".str_replace("&","%26",$lmarques[0])."\"".($getmarque==$lmarques[0]?" selected":"").">".$lmarques[0]."</option>\n";
					}
				echo "</select></h2><br /><br />\n";
			}	
		}
			
		$resprix = send_sql("SELECT * FROM refprod WHERE ID_PROD='$id_prod'".$sqlmarque." ORDER BY ORDRE");

		?>



		<div class="tabprix">

<?php

//  ***************************   TABLEAU DES PRIX   *****************************

	$afftab = true;
	if (isset($getmarque))
		if ($getmarque=='')
		{
			echo "<h3 style='color: red; font-size: 14px'>Il y a beaucoup de références dans cette catégorie, sélectionnez une marque ci-dessus</h3>\n";
			$afftab = false;
		}

	if ($afftab)
	{

	$cur_marque = "";
	while ($ligne=mysqli_fetch_array($resprix))
	{
		if ($cur_marque != $ligne['MARQUE'])
		{
			$cur_marque = $ligne['MARQUE'];
			echo "<tr><td colspan=4><h2>".$cur_marque."</h2></tr>\n";
		}
		$prod_id = $ligne['ID'];
		$prod_ref = $ligne['REF'];
		$prod_poids = $ligne['POIDS'];

		$prod_descri1 = ($ligne['MARQUE']!=""?$ligne['MARQUE']." - ":"").stripslashes($ligne['DESCRI1']).
			//	$prod_descri1 = stripslashes($ligne['DESCRI1']).
			($ligne['DESCRI2']!=""?" - ".stripslashes($ligne['DESCRI2']):"").
			($ligne['DESCRI3']!=""?" - ".stripslashes($ligne['DESCRI3']):"");
		$alt_prod_descri1 = str_replace("'","&#146;",htmlentities($prod_descri1));

		$prod_nom = $design_prod." - ".$prod_descri1;
		$prod_nom_aff = $prod_descri1;

		?>

		<div class="row">
		
			<div class="col-xs-12 col-md-6 nomprod">
				<?=$prod_nom_aff?>
			</div>
			
			<div class="col-xs-4 col-md-3 tarif"><?=($ligne['PRIX']<=0?"Nous consulter":number_format($ligne['PRIX'],2,",","")." &euro;")?></div>
		
			<?php if ($ligne['STOCK']>0) { ?>
		
			<div class="col-xs-4 col-md-2">
				<form method="post" action="<?=page_courante()?>" id="formaj<?=$prod_id?>">
					<input type="hidden" name="AJOUT_ID" value="<?=$prod_id?>" />
					<input type="hidden" name="AJOUT_NOM" value="<?=str_replace('"','&quot;',$prod_nom)?>" />
					<div class="form-group">
					<?php if ($ligne['STOCK']==999) { ?>
					<input class="form-control" type="text" id="QTE<?=$prod_id?>" name="QTE" value="1" />
					<?php } else { ?>
					<select class="form-control" id="QTE<?=$prod_id?>" name="QTE" />
					<?php for ($i=1;$i<=$ligne['STOCK'];$i++) { ?>
						<option value="<?=$i?>"><?=$i?></option>
					<?php } ?>
					</select>
					<?php } ?>
					</div>
				</form>
			</div>
			
			<div class="col-xs-1">
				<button class="btn btn-primary btn-ajpan" idprod="<?=$prod_id?>" alt="<?=$alt_title_libelle?> - <?=$alt_prod_descri1?>" title="<?=$alt_title_libelle?> - <?=$alt_prod_descri1?>">
					<i class="fa fa-shopping-cart"></i>
				</button>
			</div>
			
			<?php } else { ?>
			
			<div class="col-xs-3">Plus disponible</div>
			
			<?php } ?>
			
		</div>
		
	<?php
	
		}
	}



?>
		</div>

<?php endif; ?>


<?php

    }

	}
	
	}
	




if (isset($_REQUEST['id']))
	$exclu_prod = " AND ID!='".$_REQUEST['id']."'";
else
	$exclu_prod = '';

$ordre_offres = ' ORDER BY TOP DESC,NOUVEAU DESC';
if ($categ_prod==1)
	$ordre_offres = ' ORDER BY DESIGNATION';

$res = send_sql("SELECT * FROM produits WHERE AFFICHER=1 AND CATEG='$categ_prod'$exclu_prod$ordre_offres");
$nbprod = mysqli_num_rows($res);

if ($nbprod>0) { ?>



                    <div class="widget">
                        <div class="widget-title">
                            <h4>Toutes nos offres <?=$categ_nom?></h4>
                            <hr>
                        </div>
                        <!-- end widget-title -->

                        <div class="reviewlist review-posts row m30">

								<?php
		
$nbprod = 0;
		
while ($ligne=mysqli_fetch_array($res))
{
	$id_prod = $ligne['ID'];
	$categ_prod = $ligne['CATEG'];
	$design_prod = ucfirst(stripslashes($ligne['DESIGNATION']));
	$resume_prod = stripslashes($ligne['RESUME']);
	$details_prod = stripslashes($ligne['DETAILS']);
	$visuel = $dossier_offres.'/'.$id_prod.'/'.renomme_fichier($design_prod).'_m.jpg';
	if (!file_exists($visuel))
		$visuel = 'images/pasphoto-nb.jpg';

	$lien = '?categ='.$categ_prod.'&id='.$id_prod;
	?>
	
									<div class="post-review col-md-4 col-sm-6 col-xs-12<?=$nbprod==0?' first':''?>">
                                <div class="post-media entry" style="background-image: url(<?=$visuel?>)">
                                    <a href="<?=$lien?>" title="">
                                        <img src="images/img-offre.png" alt="<?=$design_prod?>" class="img-responsive">
                                        <div class="magnifier">
                                            <div class="review-stat">
                                                <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <!-- end review-stat -->
                                            <div class="hover-title">
                                                <span>Tech Reviews</span>
                                            </div>
                                            <!-- end title -->
                                        </div>
                                        <!-- end magnifier -->
                                    </a>
                                </div>
                                <!-- end media -->
                                <div class="post-title">
                                    <h3><a href="<?=$lien?>"><?=$design_prod?></a></h3>
                                </div>
                                <!-- end post-title -->
                            </div>
                            <!-- end post-review -->

<?php

$nbprod++;

} ?>

							
							

                        </div>

                    </div>
                    <!-- end widget -->
						  
<?php } ?>

                    <div class="widget searchwidget indexslider">
                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_01.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>How ThePhone thriller will change the way film directors & details about the Phone</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_02.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                        </div>
                                        <h3>Time to meet our new designer and developer who joined our team</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_03.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>Best office design's and workspace examples</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_04.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-primary">Interview</span></a>
                                            <a href="#"><span class="label label-warning">Web Design</span></a>
                                        </div>
                                        <h3>What you think about our new laptop its build by Apple</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->

                        <div class="large-widget m30">
                            <div class="post row clearfix">
                                <div class="col-md-5">
                                    <div class="post-media">
                                        <a href="single.html">
                                            <img alt="" src="upload/big_news_05.jpg" class="img-responsive">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="title-area">
                                        <div class="colorfulcats">
                                            <a href="#"><span class="label label-success">WordPress</span></a>
                                        </div>
                                        <h3>WordPress App for showcase galleries and Instagram users</h3>

                                        <div class="large-post-meta">
                                            <span class="avatar"><a href="author.html"><img src="upload/avatar_01.png" alt="" class="img-circle"> Kubra Karahasan</a></span>
                                            <small>&#124;</small>
                                            <span><a href="category.html"><i class="fa fa-clock-o"></i> 21 March 2016</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html#comments"><i class="fa fa-comments-o"></i> 92</a></span>
                                            <small class="hidden-xs">&#124;</small>
                                            <span class="hidden-xs"><a href="single.html"><i class="fa fa-eye"></i> 1223</a></span>
                                        </div>
                                        <!-- end meta -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                            <!-- end post -->
                        </div>
                        <!-- end large-widget -->
                    </div>
                    <!-- end widget -->

                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="ads-widget">
                                    <a href="#"><img src="upload/banner_02.jpg" alt="" class="img-responsive"></a>
                                </div>
                                <!-- end ads-widget -->
                            </div>
                            <!-- end widget -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                </div>
                <!-- end col -->



					 
<div class="modal fade" tabindex="-1" role="dialog" id="modal-mdp-ce">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Identifiez-vous !</h4>
      </div>
      <div class="modal-body">
		
			<form id="form_identifiez_vous">
				<!--
				<label for="CH_EMAIL">Votre e-mail : </label><input type="text" id="CH_EMAIL" name="CH_EMAIL" /><br />
				<label for="CH_MDP">Votre mot de passe : </label><input type="password" id="CH_MDP" name="CH_MDP" /><br />
				-->
				<div class="form-group">
					<label for="CH_CE">Votre CE : </label>
					<select name="CH_CE" class="form-control">
					<option value="">Sélectionnez votre CE</option><?php
					$resce = send_sql("SELECT * FROM listece ORDER BY NOM");
					while ($lignece=mysqli_fetch_array($resce)) echo "
					<option value=\"".$lignece['ID']."\">".stripslashes($lignece['NOM'])."</option>"
					?></select>
				</div>
				<div class="form-group">
					<label for="CH_MDPVEL">Mot de passe CE : </label>
					<input type="password" id="CH_MDPVEL" name="CH_MDPVEL" class="form-control" />
				</div>

			</form>
			
			<div id="form_identifiez_vous_txt">&nbsp;</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default bt-annule-mdp-ce" data-dismiss="modal">Annuler</button>
        <button type="button" class="btn btn-primary bt-valid-mdp-ce">Enregistrer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<script>


</script>

				 
<?php

include '_footer.php';

?>
