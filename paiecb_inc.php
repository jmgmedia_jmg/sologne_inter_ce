<?php



$CH_NUMCOM = $_REQUEST['CH_NUMCOM'];
$CH_CLIENT = $_REQUEST['CH_CLIENT'];



include 'lecture-commande-inc.php';




$net_com = $ligne_e['NET_A_PAYER'];

$email = $CH_EMAIL;




/*****************************************************************************
 *
 * "Open source" kit for CM-CIC P@iement (TM)
 *
 * File "Phase1Aller.php":
 *
 * Author   : Euro-Information/e-Commerce (contact: centrecom@e-i.com)
 * Version  : 1.04
 * Date     : 01/01/2009
 *
 * Copyright: (c) 2009 Euro-Information. All rights reserved.
 * License  : see attached document "License.txt".
 *
 *****************************************************************************/

// TPE Settings
// Warning !! CMCIC_Config contains the key, you have to protect this file with all the mechanism available in your development environment.
// You may for instance put this file in another directory and/or change its name. If so, don't forget to adapt the include path below.
require_once("CMCIC_Config.php");

// PHP implementation of RFC2104 hmac sha1 ---
require_once("CMCIC_Tpe.inc.php");



//echo "langue = ".$paiement_langue;



$sOptions = "";

// ----------------------------------------------------------------------------
//  CheckOut Stub setting fictious Merchant and Order datas.
//  That's your job to set actual order fields. Here is a stub.
// -----------------------------------------------------------------------------

// Reference: unique, alphaNum (A-Z a-z 0-9), 12 characters max
$sReference = $CH_NUMCOM;

// Amount : format  "xxxxx.yy" (no spaces)
$sMontant = $net_com;

// Currency : ISO 4217 compliant
$sDevise  = "EUR";

// free texte : a bigger reference, session context for the return on the merchant website
$sTexteLibre = "Texte Libre";

// transaction date : format d/m/y:h:m:s
$sDate = date("d/m/Y:H:i:s");

// Language of the company code
$sLangue = "FR";

// customer email
$sEmail = $CH_EMAIL;

// ----------------------------------------------------------------------------

// between 2 and 4
//$sNbrEch = "4";
$sNbrEch = "";

// date echeance 1 - format dd/mm/yyyy
//$sDateEcheance1 = date("d/m/Y");
$sDateEcheance1 = "";

// montant échéance 1 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance1 = "0.26" . $sDevise;
$sMontantEcheance1 = "";

// date echeance 2 - format dd/mm/yyyy
$sDateEcheance2 = "";

// montant échéance 2 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance2 = "0.25" . $sDevise;
$sMontantEcheance2 = "";

// date echeance 3 - format dd/mm/yyyy
$sDateEcheance3 = "";

// montant échéance 3 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance3 = "0.25" . $sDevise;
$sMontantEcheance3 = "";

// date echeance 4 - format dd/mm/yyyy
$sDateEcheance4 = "";

// montant échéance 4 - format  "xxxxx.yy" (no spaces)
//$sMontantEcheance4 = "0.25" . $sDevise;
$sMontantEcheance4 = "";

// ----------------------------------------------------------------------------

$oTpe = new CMCIC_Tpe($sLangue);     		
$oHmac = new CMCIC_Hmac($oTpe);      	        

// Control String for support
$CtlHmac = sprintf(CMCIC_CTLHMAC, $oTpe->sVersion, $oTpe->sNumero, $oHmac->computeHmac(sprintf(CMCIC_CTLHMACSTR, $oTpe->sVersion, $oTpe->sNumero)));

// Data to certify
$PHP1_FIELDS = sprintf(CMCIC_CGI1_FIELDS,     $oTpe->sNumero,
                                              $sDate,
                                              $sMontant,
                                              $sDevise,
                                              $sReference,
                                              $sTexteLibre,
                                              $oTpe->sVersion,
                                              $oTpe->sLangue,
                                              $oTpe->sCodeSociete, 
                                              $sEmail,
                                              $sNbrEch,
                                              $sDateEcheance1,
                                              $sMontantEcheance1,
                                              $sDateEcheance2,
                                              $sMontantEcheance2,
                                              $sDateEcheance3,
                                              $sMontantEcheance3,
                                              $sDateEcheance4,
                                              $sMontantEcheance4,
                                              $sOptions);

// MAC computation
$sMAC = $oHmac->computeHmac($PHP1_FIELDS);

// --------------------------------------------------- End Stub ---------------


// ----------------------------------------------------------------------------
// Your Page displaying payment button to be customized  
// ----------------------------------------------------------------------------
?>



<p>Vous avez choisi le paiement par carte bancaire pour régler votre commande n° <strong><?php 
echo $CH_NUMCOM; ?></strong><br />d'un montant de <strong><?php echo $net_com; ?></strong> &euro; (voir bon de commande ci-dessous).</p>

<p>Cliquez sur le bouton PAYER ci-dessous pour procéder au
paiement sur la plateforme sécurisée CM-CIC P@iement du <strong>Crédit Mutuel</strong>.</p>





<!-- FORMULAIRE TYPE DE PAIEMENT / PAYMENT FORM TEMPLATE -->
<form action="<?php echo $oTpe->sUrlPaiement;?>" method="post" name="PaymentRequest">
	<input type="hidden" name="version"             id="version"        value="<?php echo $oTpe->sVersion;?>" />
	<input type="hidden" name="TPE"                 id="TPE"            value="<?php echo $oTpe->sNumero;?>" />
	<input type="hidden" name="date"                id="date"           value="<?php echo $sDate;?>" />
	<input type="hidden" name="montant"             id="montant"        value="<?php echo $sMontant . $sDevise;?>" />
	<input type="hidden" name="reference"           id="reference"      value="<?php echo $sReference;?>" />
	<input type="hidden" name="MAC"                 id="MAC"            value="<?php echo $sMAC;?>" />
	<input type="hidden" name="url_retour"          id="url_retour"     value="<?php echo $oTpe->sUrlKO;?>" />
	<input type="hidden" name="url_retour_ok"       id="url_retour_ok"  value="<?php echo $oTpe->sUrlOK;?>" />
	<input type="hidden" name="url_retour_err"      id="url_retour_err" value="<?php echo $oTpe->sUrlKO;?>" />
	<input type="hidden" name="lgue"                id="lgue"           value="FR" />
	<input type="hidden" name="societe"             id="societe"        value="<?php echo $oTpe->sCodeSociete;?>" />
	<input type="hidden" name="texte-libre"         id="texte-libre"    value="<?php echo HtmlEncode($sTexteLibre);?>" />
	<input type="hidden" name="mail"                id="mail"           value="<?php echo $sEmail;?>" />
	<!-- Uniquement pour le Paiement fractionné -->
	<input type="hidden" name="nbrech"              id="nbrech"         value="<?php echo $sNbrEch;?>" />
	<input type="hidden" name="dateech1"            id="dateech1"       value="<?php echo $sDateEcheance1;?>" />
	<input type="hidden" name="montantech1"         id="montantech1"    value="<?php echo $sMontantEcheance1;?>" />
	<input type="hidden" name="dateech2"            id="dateech2"       value="<?php echo $sDateEcheance2;?>" />
	<input type="hidden" name="montantech2"         id="montantech2"    value="<?php echo $sMontantEcheance2;?>" />
	<input type="hidden" name="dateech3"            id="dateech3"       value="<?php echo $sDateEcheance3;?>" />
	<input type="hidden" name="montantech3"         id="montantech3"    value="<?php echo $sMontantEcheance3;?>" />
	<input type="hidden" name="dateech4"            id="dateech4"       value="<?php echo $sDateEcheance4;?>" />
	<input type="hidden" name="montantech4"         id="montantech4"    value="<?php echo $sMontantEcheance4;?>" />
	<!--
	<input type="submit" name="bouton"              id="bouton"         value="Connexion / Connection" />
	-->
</form>
<!-- FIN FORMULAIRE TYPE DE PAIEMENT / END PAYMENT FORM TEMPLATE -->


<?php
//   if (strlen($amount)<3) $amount="0".$amount;
$CH_MODEP="CB0";
// mise à jour du mode de paiement
send_sql("UPDATE vel_entetes SET MODEP='CB0' WHERE NUMCOM='$CH_NUMCOM' LIMIT 1");
?>



<script>
function payer() {
	document.PaymentRequest.submit();
	}
</script>




<br />
<center><input class="btn btn-primary" type="button" name="payer" value="Payer" onclick="payer()" /></center>

<br />
<br />
<br />








<?php

/*



// URL retour paiement
   $urlretour = "http://www.odyssee-creation.coop/retour_CM-CIC P@iement.php";
   
   
// Récupéré depuis la doc technique CM-CIC P@iement



$key = "5733559886971976";
// Initialisation des paramètres
$params = array(); // tableau des paramètres du formulaire
$params['vads_site_id'] = "90846247";
$trans_id = sprintf("%06d",$CH_NUMCOM);
$params['vads_amount'] = 100*$net_com; // en cents
$params['vads_currency'] = "978"; // norme ISO 4217
$params['vads_ctx_mode'] = "PRODUCTION";
$params['vads_page_action'] = "PAYMENT";
$params['vads_cust_email'] = $email;
$params['vads_action_mode'] = "INTERACTIVE"; // saisie de carte réalisée par la plateforme
$params['vads_payment_config']= "SINGLE";// $_POST['modalite'];
$params['vads_return_mode']= "GET";// Infos transmises à la page de retour boutique
$params['vads_version'] = "V2";
//$params['vads_order_info'] = $identr;

$ts = time();
$params['vads_trans_date'] = gmdate("YmdHis", $ts);

$params['vads_trans_id'] = $trans_id;
$params['vads_order_id'] = $trans_id;

// Génération de la signature
ksort($params); // tri des paramètres par ordre alphabétique
$contenu_signature = "";
foreach ($params as $nom => $valeur)
{
$contenu_signature .= $valeur."+";
}
$contenu_signature .= $key; // On ajoute le certificat à la fin
$params['signature'] = sha1($contenu_signature);
?>


<form method="POST" action="https://paiement.CM-CIC P@iement.fr/vads-payment/">

<?php

foreach($params as $nom => $valeur) {
	echo '<input type="hidden" name="' . $nom . '" value="' . $valeur . '" />'."\n";
	}


?>
<input type="submit" name="payer" value="Payer" />
</form>


*/


$CH_MODEP = "CB0";

//$pasaffbc = true;
//include 'bon-commande-inc.php';
$id_client = $session;
$mode_livraison = $CH_MODE_LIVRAISON;
$port_com = $CH_FRAIS_LIVR_HT;
include 'tab_commande_inc.php';
echo $retour_tab_commande;

?>


<div class="row" style="font-family: Arial, Helvetica, Sans-serif; font-size: 11px; color: #a0a0a0; margin: 60px 0">
	<div class="col-6 col-sm-3">
		<img class="img img-responsive" border=0 src="images/logo_cm-paiement-moy.jpg">
	</div>
	<div class="col-12 col-sm-9">
	Les transactions effectuées sur ce site sont sécurisées par le système de paiement du CREDIT MUTUEL : <b>CM-CIC P@iement</b>.
Lorsque vous saisissez vos coordonnées bancaires, vous êtes directement en liaison avec le serveur de paiement du CREDIT MUTUEL.
Le commerçant ne connaît donc pas votre numéro de carte bancaire et celui-ci n'est pas stocké sur son serveur.
En outre, toutes les informations qui sont échangées avec le CREDIT MUTUEL sont cryptées grâce au protocole SSL
et ne peuvent être interceptées ou modifiées.<br>
Avec <b>CM-CIC P@iement</b>, vous avez la garantie que le commerçant avec qui vous êtes en relation est bien référencé
auprès du CREDIT MUTUEL pour encaisser des transactions sur Internet.
A la fin de la transaction, et avant de retourner sur le site du commerçant, <b>CM-CIC P@iement</b> vous donne la possibilité d'obtenir
un ticket électronique, reprenant tous les éléments du paiement et le résultat de la demande d'autorisation
bancaire (transaction acceptée ou annulée).
	</div>
</div>

