<?php


session_start();
session_regenerate_id();
$id_client = session_id();


include '_init.php';
include 'param_coord_inc.php';



$titrepage = "Votre commande";

include '_header.php';

?>


<div class="col-md-9 col-sm-12">

	<h1>Règlement de votre commande</h1>

	<div id="commande">

		 <div id="listeitem">


	<?php


	$CH_NUMCOM = $_REQUEST['CH_NUMCOM'];
	$CH_CLIENT = $_REQUEST['CH_CLIENT'];




	// mise à jour du mode de paiement
	$sql="UPDATE vel_entetes SET MODEP='CHQ',MAJ_STOCK=1 WHERE NUMCOM='$CH_NUMCOM' AND REF_CLIENT='$CH_CLIENT' LIMIT 1";
	send_sql($sql);




	include 'lecture-commande-inc.php';




	echo "<p>Nous avons bien enregistré votre commande, et nous vous en remercions !<br />";

	if ($CH_MODE_LIVRAISON=="enlevement") echo "

	Lorsque vous viendrez chercher votre commande dans nos locaux, merci de vous munir<br />
	du bon de commande que vous pouvez imprimer à présent.\n";

	else echo "

	Veuillez imprimer le bon de commande et nous l'envoyer accompagné d'un chèque d'un montant de <b>".
	str_replace('.',',',$CH_NET_A_PAYER)." euros"."</b>\n";

	echo "</p>

	<form name='imprcomm' action='imprcomm.php' method='post' style='margin: 30px 0;'
			target='_blank'><input type='hidden' name='CH_NUMCOM' value='$CH_NUMCOM'></form>
	<input class='btn btn-primary' type='button' onClick='document.forms.imprcomm.submit()' value='Imprimer le bon de commande'>

	<br />
	<br />

	<p><b>".nl2br($coordonnees_marchand)."</b></p>
	\n";

	?>


	<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?q=47.351245,1.739981&amp;num=1&amp;gl=fr&amp;ie=UTF8&amp;ll=47.351158,1.739944&amp;spn=0.001486,0.001867&amp;t=m&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://maps.google.fr/maps?q=47.351245,1.739981&amp;num=1&amp;gl=fr&amp;ie=UTF8&amp;ll=47.351158,1.739944&amp;spn=0.001486,0.001867&amp;t=m&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">Agrandir le plan</a></small>

	<?php 

		$message_client = "<h1>Votre commande chez ".strtoupper($nom_marchand)."</h1>
	<p>Chère cliente, Cher client,</p>
	<p>Nous avons bien enregistré votre commande, et nous vous en remercions !</p>\n";

	if ($CH_MODE_LIVRAISON=="envoi")
		$message_client .= "<p>L'expédition de votre commande se fera dans les meilleurs délais
	dès que nous aurons reçu votre chèque d'un montant de <b>".str_replace('.',',',$CH_NET_A_PAYER)." euros</b>.</p>\n";
	else
		$message_client .= "<p>Lorsque vous viendrez chercher votre commande dans nos locaux, merci de vous munir<br />
	du bon de commande que vous pouvez imprimer à présent, et d'un chèque de <b>".str_replace('.',',',$CH_NET_A_PAYER)." euros</b>.</p>\n";



		$message_client .= "
	<p>Référence Internet :  $CH_NUMCOM<br />
	Numéro de client :  $CH_REF_CLIENT</p>

	<hr />
	";

		
		$id_client = $session;
		$mode_livraison = $CH_MODE_LIVRAISON;
		$port_com = $CH_FRAIS_LIVR_HT;
		include 'tab_commande_inc.php';
		$message_client.=$retour_tab_commande;

		$message_client.= "
	</table>

	<hr />
	<p><b>Facturation :</b><br />
	$CH_CIVILITE $CH_PRENOM $CH_NOM<br />
	".($CH_SOCIETE!=""?$CH_SOCIETE."<br />":"").
	"$CH_ADR1<br />
	".($CH_ADR2!=""?$CH_ADR2."<br />":"").
	"$CH_CP $CH_VILLE<br />
	$CH_PAYS<br />
	$CH_TEL</p>

	<hr />
	<p><b>Livraison :</b><br />";

		if ($CH_MODE_LIVRAISON=="envoi") 
			$message_client.= "
	$CH_CIVILITEL $CH_PRENOML $CH_NOML<br />
	".($CH_SOCIETEL!=""?$CH_SOCIETEL."<br />":"")."
	$CH_ADR1L<br />
	".($CH_ADR2L!=""?$CH_ADR2L."<br />":"")."
	$CH_CPL $CH_VILLEL<br />
	$CH_PAYSL</p>";
		else
			$message_client.= "
	Retrait dans nos locaux<br /><br /></p>\n";

		$message_client.= "
	<hr />";


		if ($CH_OBSCLI!="") 
			$message_client.= "<p><b>Observations :</b><br />$CH_OBSCLI</p><hr />";

		$message_client.="<p>Nous vous remercions d'avoir passé votre commande chez $nom_marchand.</p>";
		$titre=$nom_marchand." : votre commande $CH_NUMCOM";
		
		envoi_message($CH_EMAIL,$titre,$message_client,$email_marchand,"html");
		envoi_message($email_commandes,"Nouvelle commande n° $CH_NUMCOM par CHEQUE",$message_client,$CH_EMAIL,"html");
		
		
		

	?>


		</div>

	</div>

</div>
<!-- end col -->



				 
<?php

include '_footer.php';

?>
