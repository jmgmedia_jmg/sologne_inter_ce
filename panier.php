<?php


include '_header.php';


if (isset($_POST['DELID'])) {
	$res =  send_sql("DELETE FROM vel_commandes WHERE SESSION='$id_client' AND IDPROD='".$_POST['DELID']."' LIMIT 1");
	}



$erreur = '';


ob_start();

// ******* CALCUL DES LIGNES DU PANIER **********

$res = send_sql("SELECT c.*	FROM vel_commandes c 
	INNER JOIN refprod r ON r.ID=c.IDPROD
	WHERE SESSION='$id_client'");

$nbprodpan = mysqli_num_rows($res); // Nbre total d'enregistrements

$total_com = 0;
$poids_com = 0;
$expediable = 1;

if ($nbprodpan>0)
{
	for ($i=0;$i<$nbprodpan;$i++)
	{
		$ligne = mysqli_fetch_array($res);
		$aff_idprod = $ligne['IDPROD'];
		$aff_ref = $ligne['REF'];
		$aff_nom = stripslashes($ligne['NOM']);
		$alt_title_libelle =  str_replace("'","&#146;",htmlentities($aff_nom));
		$aff_puht = $ligne['PUHT'];
		$aff_qte = $ligne['QTE'];
		$aff_totalht = $ligne['TOTALHT'];
		$poids_com += $ligne['POIDS_TOTAL'];
		if ($ligne['POIDS_TOTAL']==0)
			$expediable = 0;


		$lien_prod = "";
		
		?>
			<tr>
				<td class="pandes"><a href="<?=$lien_prod?>"><?=$aff_nom?></a></td>
				<td class="panqte"><?=$aff_qte?></td>
				<td class="panpri"><?=number_format($aff_puht,2,',',' ')?> &euro;</td>
				<td class="panpri"><?=number_format($aff_totalht,2,',',' ')?> &euro;</td>
				<td class="pansup"><a href="javascript:supprime(<?=$aff_idprod?>)" title="Supprimer ce produit"><i class="fa fa-trash"></i></a></td>
			</tr>
		
		<?php
		
		$total_com = $total_com+$aff_totalht;
		
		} ?>
	
		<tr class="pansto">
			<td colspan="3" style="text-align: right">Sous-total :</td>
			<td class="panpri"><?=number_format($total_com,2,',',' ')?> &euro;</td>
			<td class="pansup">&nbsp;</td>
			</tr>
			<tr class="pantot gro">
			<td colspan="3" style="text-align: right">Montant TTC hors frais de livraison</td>
			<td class="panpri"><?=number_format($total_com,2,',',' ')?> &euro;</td>
			<td class="pansup">&nbsp;</td>
		</tr>

		<?php
		
		$aff_panier = ob_get_clean();

	}
	
else 

	$aff_panier="<br />Votre panier est vide...";




?>



<script>

function valide_form_comm() {
    document.form_comm.method='post';
    document.form_comm.action="ident.php?suite=coordonnees";
    document.form_comm.submit();
    }


function supprime(delid) {
    document.listeitem.DELID.value=delid;
    document.listeitem.method='post';
    document.listeitem.action='panier.php';
    document.listeitem.submit();
    }

</script>



<div class="col-md-9 col-sm-12">




<div id="panier">

    <form id="listeitem" name="listeitem"><input name="DELID" type="hidden">

	<?php

	if ($erreur!="") echo "<p class='descri'><font color=red><b>$erreur</b></font></p>\n";


	if ($nbprodpan>0) { ?>

	<table class="table table_panier">
		<thead>
			<tr>
				<th class="pandes">Désignation du produit</th>
				<th class="panqte">Qté</th>
				<th class="panpru">Prix unitaire</th>
				<th class="panprt">Prix total</th>
				<th class="pansup">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		
		<?=$aff_panier?>
		
		</tbody>
	</table>
	
	<?php } ?>

	</form>
		
	
<?php

if ($expediable) {
	if ($poids_com>1)
		echo "<p class='texte_retour'>Cette commande ne peut pas être expédiée ( poids total supérieur à 1 kg ) mais vous pourrez venir la chercher dans nos locaux.</p>";
	}
else
	echo "<p class='texte_retour'>Votre panier contient des articles qui ne peuvent pas être expédiés, mais vous pourrez venir chercher la commande dans nos locaux.</p>";




if ($nbprodpan>0) { ?>

<div id="totalgene">
	<form name="form_comm" onsubmit="javascript:valide_form_comm()">
		<input type="hidden" name="CH_TOTAL_COM" value="<?=$total_com?>" />
		<input type="hidden" name="CH_POIDS_COM" value="<?=$poids_com?>" />
		<input type="hidden" name="CH_EXPEDIABLE" value="<?=$expediable?>" />
		<input class="btn btn-primary" type="submit" value="Passer ma commande" />
	</form>
</div>

<?php } ?>


</div>


</div>
<!-- end col -->



				 
<?php

include '_footer.php';

?>
