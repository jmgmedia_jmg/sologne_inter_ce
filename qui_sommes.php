<?php

$use_fancybox = true;

include '_header.php';

$ligne_intro = "Les news de Sologne Inter CE";

?>

<div class="col-md-9 col-sm-12">

	<div class="widget-title">
		 <h4>L'association Sologne Inter CE</h4>
		 <hr>
	</div>

	<br />

<p>Nous sommes une association régie par la loi du 1er juillet
1901 et le décret du 16 août 1901 ayant pour titre : INTER CE
</p>
<p>Association de liaison des comités d’entreprises et similaires. </p>
<p>Son sigle est : SOLOGNE INTER CE </p>
<p><strong>Siège social :</strong></p>
<p>147 rue du Président WILSON<br>
BP 30084 - 41200 ROMORANTIN- LANTHENAY</p>
<p><strong>L’association a pour objet de : </strong></p>
<ul>
<li>créer, coordonner et développer entre CE et similaires les activités qui leurs sont habituelles, </li>
<li>offrir la possibilité au plus grand nombre de salariés et à
leurs familles, et notamment à ceux des petites entreprises avec
ou sans comité d’entreprise, ainsi qu’aux retraités et
préretraités de toutes origines professionnelles, de
participer à la vie associative pour des réalisations
communes, </li>
<li>fournir aux comités d'entreprises et similaires la
formation, l’information, les conseils techniques
nécessaires aux activités qu’ils organisent et animent,
</li>
<li>organiser des rencontres sportives et culturelles, des
sorties, des spectacles, des soirées avec ou sans restauration,
</li>
</ul>
<p><strong>Droit d'entrée :</strong></p>
<p>Un droit d’entrée est demandé, dont le montant est fixé à 8,00 euros pour l’année 2018 :</p>
<ul>
<li>soit 3,00 € de cotisation annuelle par adhèrent révisable chaque année, </li>
<li>soit 5,00 € de droit d’entrée pour l’achat de billetterie en commun. </li>
</ul>
<?php
//$res=send_sql("SELECT DISTINCT NOM FROM newsletter WHERE CE='1' ORDER BY NOM");
$res=send_sql("SELECT DISTINCT NOM FROM listece ORDER BY NOM");
$nbenr = mysqli_num_rows($res);
?>
<p><strong>L'association regroupe <?=$nbenr?> comités d'entreprises :</strong></p>
<p><?php
while ($ligne=mysqli_fetch_array($res)) echo stripslashes($ligne['NOM'])."<br>\n";
?></p>


	
	

</div>
                <!-- end col -->



<?php

include '_footer.php';

?>
