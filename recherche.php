<?php

include '_header.php';



$search = '';
if (isset($_POST['CH_RECH']))
	$search = $_POST['CH_RECH'];


?>




<div class="col-md-9 col-sm-12">

	<div class="widget-title">
		 <h4>Votre recherche</h4>
		 <hr>
	</div>

		<div class="pgnorm">

			<br />
			<p>Vous avez cherché : <font color="#FF930D"><b><?php echo $search; ?></b></font></p>


<?php

// -------------------------- LES OFFRES ---------------------------
$sql="SELECT categories.ID AS CATID,LIBELLE,DESIGNATION,produits.ID AS PRID 
	FROM produits LEFT JOIN categories ON CATEG=categories.ID 
	WHERE AFFICHER=1 AND MATCH(DESIGNATION,RESUME,DETAILS) AGAINST('$search*' IN BOOLEAN MODE)";
$res=send_sql($sql);
$liste_resultats="";
while($ligne=mysqli_fetch_array($res)) {
        $res_categ=StripSlashes($ligne['LIBELLE']);
        $res_designation=StripSlashes($ligne['DESIGNATION']);
        $res_id=$ligne['PRID'];
        $res_idcateg=$ligne['CATID'];
        $liste_resultats.="<p style=\"font-size: 12px; margin-top: 0; margin-bottom: 0\"><b><a href='offres.php?categ=$res_idcateg&id=$res_id'>$res_designation</a></b> ($res_categ)</p>
              <hr style=\"width: 400px; margin-left: 0; margin-top: 0; margin-bottom: 0\" size='1px' align='left'>\n";
        }

if ($liste_resultats!="") echo "
   <h2 style=\"margin-bottom: 10px\">Résultats dans les offres</h2>
   $liste_resultats\n";



// -------------------------- LES PRODUITS ---------------------------
/*
$sql = "SELECT r.*,r.ID AS RID,LIBELLE,DESIGNATION,c.ID AS CATID FROM refprod r INNER JOIN produits p ON p.ID=r.ID_PROD
	LEFT JOIN categories c ON p.CATEG=c.ID 
	WHERE r.REF LIKE '%$search%' OR r.MARQUE LIKE '%$search%' OR r.DESCRI1 LIKE '%$search%'";
$res=send_sql($sql);
$liste_resultats="";
while($ligne=mysqli_fetch_array($res)) {
        $res_marque = StripSlashes($ligne['MARQUE']);
        $res_descri1 = StripSlashes($ligne['DESCRI1']);
        $res_descri2 = StripSlashes($ligne['DESCRI2']);
        $res_descri3 = StripSlashes($ligne['DESCRI3']);
        $res_id = $ligne['RID'];
        $res_categ = StripSlashes($ligne['LIBELLE']);
        $res_designation = StripSlashes($ligne['DESIGNATION']);
        $res_idcateg = $ligne['CATID'];
        $liste_resultats.="<p style=\"font-size: 12px; margin-top: 0; margin-bottom: 0\"><b><a href='offres.php?categ=$res_idcateg&id=$res_id'>$res_designation</a></b> ($res_categ)</p>
              <hr style=\"width: 400px; margin-left: 0; margin-top: 0; margin-bottom: 0\" size='1px' align='left'>\n";
        }

if ($liste_resultats!="") echo "
   <h2 style=\"margin-bottom: 10px\">Résultats dans les offres</h2>
   $liste_resultats\n";
*/

// -------------------------- LES ACTUALITES ---------------------------
$sql="SELECT DATE,TITRE,REF FROM actu WHERE AFFICHER=1 AND MATCH(TITRE,ACCROCHE,RESUME,TEXTE) AGAINST('$search*' IN BOOLEAN MODE)";
$res=send_sql($sql);
$liste_resultats_2="";
while ($ligne=mysqli_fetch_array($res)) {
        $res_date=datefr($ligne['DATE']);
        $res_titre=StripSlashes($ligne['TITRE']);
        $res_ref=$ligne['REF'];
        $liste_resultats_2.="<p style=\"font-size: 12px; margin-top: 0; margin-bottom: 0\"><b><a href='actu.php?ref=$res_id'>$res_titre</a></b> (actu du $res_date)</p>
              <hr style=\"width: 400px; margin-left: 0; margin-top: 0; margin-bottom: 0\" size='1px' align='left'>\n";
        }

if ($liste_resultats_2!="") echo "
   <h2 style=\"margin-bottom: 10px\">Résultats dans les actualités</h2>
   $liste_resultats_2\n";


if ($liste_resultats==""&&$liste_resultats_2=="") echo "<p style=\"color: red\">Aucun résultat pour cette recherche</p>\n";

?>

			 
					 
					 
</div>
<!-- end col -->



<?php

include '_footer.php';

?>
