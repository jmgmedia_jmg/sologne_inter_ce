<!-- API Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAavDUrEM1oOixC4mUrNekeBIStb3pOaEY" type="text/javascript"></script>
<script type="text/javascript">

var infowindow;

(function(){
	
	google.maps.Map.prototype.markers=new Array();
	google.maps.Map.prototype.addMarker=function(marker){
		this.markers[this.markers.length]=marker;
		};
	google.maps.Map.prototype.getMarkers=function(){
		return this.markers
		};
	google.maps.Map.prototype.clearMarkers=function(){
		if(infowindow){
			infowindow.close();
		}
		for(var i=0;i<this.markers.length;i++){
			this.markers[i].set_map(null);
		}};
	})();

function initialize(){
	var latlng = new google.maps.LatLng(47.359947, 1.743575);
	var myOptions={zoom:10,center:latlng,mapTypeId:google.maps.MapTypeId.ROADMAP};
	map = new google.maps.Map(document.getElementById("map"),myOptions);
	
	<?php
	$cpt = 0;
	$res_mag = send_sql("SELECT ID, COORD, NOM FROM remises WHERE COORD!= '' AND COORD!='unknown'");
	while ($ligne_mag=mysqli_fetch_array($res_mag)) { ?>
	
	var latlng = new google.maps.LatLng<?=$ligne_mag['COORD']?>;
	var marker<?=$cpt?> = new google.maps.Marker({position:latlng,map:map,title:"<?=stripslashes($ligne_mag['NOM'])?>"});
	google.maps.event.addListener(marker<?=$cpt?>,"click",function(){
		if(infowindow)infowindow.close();
		$.ajax({
			url: 'revendeurs-maps-script-ajax.php',
			data: 'ident=<?=$ligne_mag['ID']?>',
			success: function(retour) {
				infowindow = new google.maps.InfoWindow({content: retour});
				infowindow.open(map,marker<?=$cpt?>);
				}
			});

		
	});

	map.addMarker(marker<?=$cpt?>);
	
	<?php $cpt++; } ?>
	
	console.log(map.getMarkers());
	console.log(map.getMarkers());
}



$(document).ready(function()
{
	initialize();
});



</script>
