<?php

$use_fancybox = true;

$include_script = 'remises-maps-script.php';

$api_google_maps = true;


include '_header.php';

$ligne_intro = "Les news de Sologne Inter CE";

?>

<div class="col-md-6 col-sm-12">

	<div class="widget-title">
		 <h4>Les remises chez les commerçants</h4>
		 <hr>
	</div>

	<br />

	<p>Sur présentation de la carte d'adhérent Sologne inter CE, vous pouvez bénéficier de remises intéressantes chez les commerçants suivants :</p>

	<div id="map" style="height: 400px"></div>
	<br />
	<br />

	<?php

	$ville_courante = '';
	$res = send_sql("SELECT * FROM remises ORDER BY VILLE,NOM");
	while ($ligne=mysqli_fetch_array($res))
	{
		 $rem_nom = stripslashes($ligne['NOM']);
		 $rem_ville = stripslashes($ligne['VILLE']);
		 $rem_adresse = stripslashes(($ligne['ADRESSE']!=''?$ligne['ADRESSE'].' - ':'').($ligne['CODEPOST']!=''?$ligne['CODEPOST'].' ':'').$ligne['VILLE']);
		 $rem_remise = stripslashes($ligne['REMISE']);
		 $rem_obs = nl2br(stripslashes($ligne['OBS']));
		 $rem_activite = majuscules(stripslashes($ligne['ACTIVITE']));
		 
		 if ($rem_ville!=$ville_courante) { ?>
		 
			<div class="widget-title">
				 <h4><?=$rem_ville?></h4>
				 <hr>
			</div>

			<?php
			
			$ville_courante = $rem_ville;
			
			}

		?>

			  <div class="widget searchwidget indexslider">
					<div class="large-widget m30">
						 <div class="post row clearfix">
							  <div class="col-xs-12">
									<div class="title-area">
										 <div class="colorfulcats">
											  <span class="label label-bleu"><?=$rem_activite?></span>
										 </div>
										 <h3><?=$rem_nom?> &nbsp; <label class="label label-primary label-lg"><?=$rem_remise?></label></h3>
										 
										<p><?=$rem_obs?></p>
										<p><?=$rem_adresse?></p>
												
									</div>
									<!-- /.pull-right -->
							  </div>
						 </div>
						 <!-- end post -->
					</div>
					<!-- end large-widget -->

			  </div>
			  <!-- end widget -->

	<?php } ?>

</div>
<!-- end col -->


<div class="col-md-3 col-sm-12">

	<?php include '_coldroite.php'; ?>
	
</div>
<!-- end col -->



<?php

include '_footer.php';

?>
