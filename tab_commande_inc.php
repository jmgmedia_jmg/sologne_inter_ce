<?php

// Cette variable est retournée pour affichage/impression
$retour_tab_commande="";


if (!isset($panier)) $panier=false;

$c1=450;
$c2=80;
$c3=60;
$c4=80;
$c5=150;


/*
$res=send_sql("SELECT c.*,p.SOUS_FAMILLE AS PSF,sf.REFER,sf.LIBELLE FROM vel_commandes c 
	INNER JOIN vel_produits p ON p.ID=c.IDPROD
	INNER JOIN vel_sous_familles sf ON p.SOUS_FAMILLE=sf.ID
	WHERE SESSION='$id_client'");
*/
$res = send_sql("SELECT c.*	FROM vel_commandes c 
	INNER JOIN refprod r ON r.ID=c.IDPROD
	WHERE SESSION='$id_client'");

$nbenr = mysqli_num_rows($res); // Nbre total d'enregistrements
if ($nbenr>0) {
	$retour_tab_commande = "<table id='tabpanier' cellspacing=0>
			<tr>
			<th class='pandes'>Désignation du produit</th>
			<th class='panqte'>Quantité</th>
			<th class='panpru'>Prix unitaire</th>
			<th class='panprt'>Prix total</th>
			</tr>\n";

	$total_com = 0;
	$tva_com = 0;
	$poids_com = 0;
	$nbenvoi = 0;

	$paypal_liste="";
	$paypal_cpt=0;

	for ($i=0;$i<$nbenr;$i++) {
		$ligne=mysqli_fetch_array($res);
		$aff_idprod=$ligne['IDPROD'];
		$aff_ref=$ligne['REF'];
		$aff_nom = stripslashes($ligne['NOM']);
		$alt_title_libelle= str_replace("'","&#146;",htmlentities($aff_nom));
		$aff_puht=$ligne['PUHT'];
		$aff_qte=$ligne['QTE'];
		$aff_totalht=$ligne['TOTALHT'];
		$tva_com += $ligne['TVA'];
		$poids_com += $ligne['POIDS_TOTAL'];

		$lien_prod = "";
		// affichage de la photo
		/*
		$nomphoto = "images/pasphoto1.png";
		$selp=send_sql("SELECT NOM FROM vel_photos WHERE ID_SOUS_FAMILLE='$aff_idsf' ORDER BY ORDRE");
		if ($lignep = mysqli_fetch_array($selp)) { 
			$nomphoto_tmp = "catalogue/$identr/$aff_idsf/cata_".$lignep[0];
			//echo $nomphoto_tmp;
			if (file_exists($nomphoto_tmp))
				$nomphoto = $nomphoto_tmp;
			}
		*/
		$retour_tab_commande.="
			<tr>".
			/*
			"<td class='panpic'><a href='$lien_prod' alt=\"$alt_title_libelle\" title=\"$alt_title_libelle\"><img 
				border=0 src='$nomphoto' height=45 alt=\"$alt_title_libelle\" title=\"$alt_title_libelle\" /></a></td>".
			*/
			"<td class='pandes'><a href='$lien_prod'>$aff_nom</a></td>
			<td class='panqte'>$aff_qte</td>
			<td class='panpri'>".number_format($aff_puht,2,',','')." &euro;</td>
			<td class='panpri'>".number_format($aff_totalht,2,',',' ')." &euro;</td>
			</tr>\n";


		$total_com  += $aff_totalht;
		
		}
		
	$retour_tab_commande.="
		<tr class='pansto'>
		<td colspan=3 style='text-align: right'>Sous-total :</td>
		<td class='panpri'>".number_format($total_com,2,',',' ')." &euro;</td>
		</tr>\n";
	
/*	if ($montant_remise>0) $retour_tab_commande .= "
                  <tr>
                    <td height=25 width=".($c1+$c2+$c3)." colspan=4 align=\"right\"><b>Remise
                    $aff_REMISE %</b>&nbsp;&nbsp;</td>
                    <td height=25 width=$c4 align='right'><b>- ".number_format($montant_remise,2,',','')."</b></td>
                  </tr>";
*/

	if ($mode_livraison!="enlevement")
		$retour_tab_commande .= "
		<tr class='pansto'>
			<td colspan=3 style='text-align: right'>Frais de port :</td>
			<td class='panpri'>".($port_com>0?number_format($port_com,2,',','')." &euro;":
			"<font color=red>Offert</font>")."</td>
		</tr>\n";
		
	$retour_tab_commande .= "
		<tr class='pantot gro'>
			<td colspan=3 style='text-align: right'>Total TTC :</td>
			<td class='panpri'>".number_format($total_com+$port_com,2,',',' ')." &euro;</td>
		</tr>

	</table>";

		
	}

else $retour_tab_commande = "<br />Impossible de traiter cette commande, il n'y a pas de produits sélectionnés !<br /><br />";

//echo $retour_tab_commande;

?>
